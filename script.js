function changeTitle() {
  let title = document.getElementById("title");
  console.log(title);
  title.innerHTML = `Selamat Malam`;
}

function submit() {
  let nama = document.getElementsByName("nama")[0];
  console.log(nama.value);
  let result = document.getElementById("result");
  result.innerHTML = `Selamat datang ${nama.value}`;
}

const deskripsi = document.getElementsByClassName("description")[0];
deskripsi.style.backgroundColor = "red";
deskripsi.style.color = "white";

const deskripsi2 = document.getElementsByClassName("description")[1];
deskripsi2.style.backgroundColor = "yellow";
deskripsi2.style.color = "black";

const subtitle = document.createElement("h2");
subtitle.textContent = "ini adalah subtitle";
document.body.append(subtitle);

const desc = document.querySelector(".description");
desc.style.color = "green";
desc.style.backgroundColor = "black";

const desc2 = document.querySelectorAll(".description")[1];
console.log(desc2);
desc2.innerHTML = "<h1>tulisan ini diubah melalui selector all</h1>";

class Cards {
  constructor() {
    this.options = [
      {
        name: "gambar 1",
        url:
          "https://image.freepik.com/free-vector/realistic-sale-background-with-ripped-paper_23-2148860761.jpg",
      },
      {
        name: "gambar 2",
        url:
          "https://image.freepik.com/free-psd/hard-cover-book-mockup_358694-19.jpg",
      },
      {
        name: "gambar 3",
        url:
          "https://image.freepik.com/free-psd/frame-mockup-living-room-interior_1150-34724.jpg",
      },
    ];
  }

  getCards() {
    const t = this;
    return this.options.forEach((opt) => {
      const img = document.createElement("img");
      img.src = opt.url;
      img.className = "m-1";
      img.onclick = function () {
        t.choosePic(opt.name);
      };
      document.getElementById("cards").append(img);
    });
  }

  choosePic(binatang) {
    document.getElementsByClassName(
      "result"
    )[0].innerHTML = `Saya mengklik ${binatang}`;
  }
}

const kartu = new Cards();
kartu.getCards();
